#! /bin/bash

cell=$1
nvsim=../nvsim/nvsim
pipe=tmpfile
oldformat="%-10s %-10s %-14s %-15s %-14s %-14s %-14s %-14s %-8s\n"

if [ ! $# -eq 1 ]
then
    echo "Usage:"
    echo "  $0 [mem_cell_file.cell]"
    exit 1
fi

printf  "%10s,%10s,%14s,%15s,%14s,%14s,%14s,%14s,%10s\n" \
    "Capacity" "Word Width" "Total Area" "Area Efficiency"\
    "Read Latency" "Write Latency" \
    "Read Energy" "Write Energy" "Leakage"

for kilo_words in 1 2 4 8 16 32 64 128 #256 512
do
    capacity=$((4 * $kilo_words))

    if [ $capacity -lt 32 ] 
    then
        banks="1x1, 1x1"
    else
        banks="2x2, 1x2"
    fi

    cfg="-DesignTarget: RAM
-EnablePruning: Yes
-ProcessNode: 28
-Capacity (KB): $capacity
-WordWidth (bit): 32
-DeviceRoadmap: LOP
-LocalWireType: LocalAggressive
-LocalWireRepeaterType: RepeatedNone
-LocalWireUseLowSwing: No
-GlobalWireType: GlobalAggressive
-GlobalWireRepeaterType: RepeatedNone
-GlobalWireUseLowSwing: No
-Routing: H-tree
-InternalSensing: true
-MemoryCellInputFile: $cell 
-Temperature (K): 350
-BufferDesignOptimization: area
-ForceBank (Total AxB, Active CxD): $banks //1x4, 1x1
"
    echo "$cfg" > $pipe

    $nvsim $pipe \
        | grep \
        -e 'Data Width' \
        -e 'Capacity' \
        -e 'Total Area' \
        -e 'Area Efficiency' \
        -e '\(Read\|Write\) Latency' \
        -e '\(Read\|Write\) Dynamic Energy' \
        -e '- Leakage Power' \
        | awk '{print $NF}' \
        | paste -s \
        | awk '{printf "%10s,%10s,%14s,%15s,%14s,%14s,%14s,%14s,%10s\n", $1, $2, $3, $4, $5, $6, $7, $8, $9}'
done
