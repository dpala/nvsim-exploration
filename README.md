# Design Space Exploration with NVSim

This repository contains the `.cell` and `.cfg` files used for the design-space exploration with **NVSim**, in the paper: 
**Freezer: A Specialized NVM Backup Controller for Intermittently-Powered Systems** [^fn]

To obtain the values in Table VI of the paper, use the `./explore_mem.sh` script, with the desired `.cell` file.

```
> ./explore_mem.sh cfgs/custom_sram.cell
```

The script will output a coma separated table, like the one reported below.

|  Capacity|Word Width|    Total Area|Area Efficiency|  Read Latency| Write Latency|   Read Energy|  Write Energy|   Leakage  |
|----------|----------|--------------|---------------|--------------|--------------|--------------|--------------|------------|
|       4KB|  (4Bytes)|  4431.465um^2|        88.697%|     709.557ps|     709.557ps|       0.219pJ|       0.111pJ| 199.723uW  |
|       8KB|  (4Bytes)|  8539.367um^2|        92.058%|     904.006ps|     904.006ps|       0.374pJ|       0.198pJ| 394.753uW  |
|      16KB|  (4Bytes)| 16831.501um^2|        93.410%|       1.282ns|       1.282ns|       0.703pJ|       0.215pJ| 771.951uW  |
|      32KB|  (4Bytes)| 34160.731um^2|        92.049%|     919.262ps|     913.147ps|       1.664pJ|       1.175pJ|   1.579mW  |
|      64KB|  (4Bytes)| 67339.482um^2|        93.392%|       1.301ns|       1.295ns|       2.500pJ|       1.388pJ|   3.088mW  |
|     128KB|  (4Bytes)|132079.349um^2|        95.230%|       1.744ns|       1.720ns|       4.445pJ|       2.378pJ|   6.144mW  |
|     256KB|  (4Bytes)|262432.719um^2|        95.856%|       2.523ns|       2.497ns|       7.226pJ|       2.825pJ|  12.147mW  |
|     512KB|  (4Bytes)|516853.522um^2|        97.342%|       3.504ns|       3.412ns|      13.329pJ|       4.835pJ|  24.182mW  |

The `./explore_mem.sh` script assumes that the `nvsim` executable is located in the path: `../nvsim/nvsim`.
To change the `nvsim` path, modify the `$nvsim` variable declared at the beginning of the `./explore_mem.sh` script.

```
nvsim=/path/to/nvsim
```

## Cache Dynamic Energy

To obtain the values for cache hit and miss dynamic energy, reported in Table VII of the paper, run `nvsim` with the `./cfgs/custom_cache.cfg` config file.
To obtain all the values in Table VII, modify the `-Capacity (KB)` parameter in `./cfgs/custom_cache.cfg` with the desired size.

```
> nvsim ./cfgs/custom_cache.cfg
```

The values reported in Table VII of the paper, are obtained by summing the contributions of the *CACHE DATA ARRAY* and the *CACHE TAG ARRAY*.

The summary values:

 - Cache Hit Dynamic Energy
 - Cache Miss Dynamic Energy
 - Cache Write Dynamic Energy

are expressed in *nJ* and lack the required precision, thus they are not used in the paper.

[^fn]: https://ieeexplore.ieee.org/document/9200760
